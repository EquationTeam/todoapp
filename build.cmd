@echo off
setlocal

    for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
    set "DEL=%%a"
    )

    :: Project
    set PROJECT_NAME=TodoApp
    set PROJECT_PATH=%~dp0%PROJECT_NAME%

    :: Localisation MSBuild.exe
    set MSBUILD_PATH=C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319
    
    echo.
    call :colorEcho 0e "Compiling %PROJECT_NAME% for .NETApp,Mvc=v4.0"
    echo.

    call %MSBUILD_PATH%\MSBuild.exe "%PROJECT_PATH%.sln" /p:DeployOnBuild=true /verbosity:quiet

    if %ERRORLEVEL% NEQ 0 (
        echo.
        goto :error
    )

    :success
    call :colorEcho 0a "Compilation succeeded."
    exit /b 0

    :error
    call :colorEcho 0c "Compilation failed."
    exit /b 1

    :colorEcho
    echo off
    <nul set /p ".=%DEL%" > "%~2"
    findstr /v /a:%1 /R "^$" "%~2" nul
    del "%~2" > nul 2>&1i

endlocal