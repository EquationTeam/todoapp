﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using TodoApp.DAL;

namespace TodoApp.Helper
{
    /// <summary>
    /// Helper for progressbar colorize
    /// </summary>
    public static class ProgressHelper
    {
        /// <summary>
        /// Helper for progress bar
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <returns></returns>
        public static MvcHtmlString ProgressBar(this HtmlHelper htmlHelper, int progress, Priority priority)
        {
            return ProgressBar(htmlHelper, progress, priority, (IDictionary<string, object>)null);
        }

        /// <summary>
        /// Helper for progress bar
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        public static MvcHtmlString ProgressBar(this HtmlHelper htmlHelper, int progress, Priority priority, object htmlAttributes)
        {
            return ProgressBar(progress, priority, htmlAttributes);
        }

        /// <summary>
        /// Helper for progress bar
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        public static MvcHtmlString ProgressBarFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, Priority priority, object htmlAttributes)
            where TModel : class
        {
            TProperty value = htmlHelper.ViewData.Model == null 
                ? default(TProperty) 
                : expression.Compile()(htmlHelper.ViewData.Model);
            int progress = value == null ? 0 : int.Parse(value.ToString());


            return ProgressBar(progress, priority, htmlAttributes);
        }

        /// <summary>
        /// Processing helper for progress bar
        /// </summary>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        private static MvcHtmlString ProgressBar(int progress, Priority priority, object htmlAttributes)
        {
            var div = new TagBuilder("div");
            if (htmlAttributes != null)
            {
                div.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }
            div.AddCssClass("progress-bar");
            div.MergeAttribute("class", "progress-bar");
            div.MergeAttribute("role", "progressbar");
            div.MergeAttribute("aria-valuenow", progress.ToString());
            div.MergeAttribute("aria-valuemin", "0");
            div.MergeAttribute("aria-valuemax", "100");
            div.MergeAttribute("style", string.Format("width:{0}%", progress));
            div.InnerHtml = progress.ToString() + "%";

            if (progress != 100)
            {
                switch (priority)
                {
                    case Priority.LOW:
                        div.AddCssClass("progress-bar-default");
                        break;
                    case Priority.NORMAL:
                        div.AddCssClass("progress-bar-warning");
                        break;
                    case Priority.HIGH:
                        div.AddCssClass("progress-bar-danger");
                        break;
                    default:
                        break;
                }
            }
            else
            {
                div.AddCssClass("progress-bar-success");
            }

            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }
    }
}