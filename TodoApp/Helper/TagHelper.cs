﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using TodoApp.DAL;

namespace TodoApp.Helper
{
    /// <summary>
    /// Various html help
    /// </summary>
    public static class TagHelper
    {
        /// <summary>
        /// Tr helper
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <returns></returns>
        public static MvcForm Tr(this HtmlHelper htmlHelper, int progress, Priority priority)
        {
            return Tr(htmlHelper, progress, priority, (IDictionary<string, object>)null);
        }

        /// <summary>
        /// Tr helper
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        public static MvcForm Tr(this HtmlHelper htmlHelper, int progress, Priority priority, object htmlAttributes)
        {
            return Tr(htmlHelper, progress, priority, htmlAttributes);
        }

        /// <summary>
        /// Tr helper
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="progress">Progress of todo</param>
        /// <param name="priority">Priority of todo</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        public static MvcForm Tr(this HtmlHelper htmlHelper, int progress, Priority priority, IDictionary<string, object> htmlAttributes)
        {
            var div = new TagBuilder("tr");
            div.MergeAttributes(htmlAttributes);
            if (progress != 100)
            {
                switch (priority)
                {
                    case Priority.LOW:
                        div.AddCssClass("bg-default");
                        break;
                    case Priority.NORMAL:
                        div.AddCssClass("bg-warning");
                        break;
                    case Priority.HIGH:
                        div.AddCssClass("bg-danger");
                        break;
                }
            }
            else
            {
                div.MergeAttribute("class", "bg-success");
            }

            htmlHelper.ViewContext.Writer.Write(div.ToString(TagRenderMode.StartTag));
            MvcForm thetd = new MvcForm(htmlHelper.ViewContext);

            return thetd;
        }

        /// <summary>
        /// Format post with line
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        public static MvcHtmlString RawLine(this HtmlHelper htmlHelper, string post)
        {
            return MvcHtmlString.Create(post.Replace("\n", "<br />"));
        }
    }
}