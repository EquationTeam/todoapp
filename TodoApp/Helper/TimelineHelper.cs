﻿using System.Collections.Generic;
using System.Web.Mvc;
using TodoApp.DAL;

namespace TodoApp.Helper
{
    /// <summary>
    /// Helper for timeline bagde
    /// </summary>
    public static class TimelineHelper
    {
        /// <summary>
        /// Create bagde with icon and colorize
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="timeline">The timeline</param>
        /// <returns></returns>
        public static MvcHtmlString Badge(this HtmlHelper htmlHelper, Timeline timeline)
        {
            return Badge(htmlHelper, timeline, (IDictionary<string, object>)null);
        }

        /// <summary>
        /// Create bagde with icon and colorize
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="timeline">The timeline</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        public static MvcHtmlString Badge(this HtmlHelper htmlHelper, Timeline timeline, object htmlAttributes)
        {
            return Badge(htmlHelper, timeline, htmlAttributes);
        }

        /// <summary>
        /// Create bagde with icon and colorize
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="timeline">The timeline</param>
        /// <param name="htmlAttributes">The HTML attribute.</param>
        /// <returns></returns>
        public static MvcHtmlString Badge(this HtmlHelper htmlHelper, Timeline timeline, IDictionary<string, object> htmlAttributes)
        {
            var div = new TagBuilder("div");
            var child = new TagBuilder("i");
            div.MergeAttributes(htmlAttributes);
            div.AddCssClass("timeline-badge");
            switch (timeline.Type)
            {
                case EntryType.ADD:
                    div.AddCssClass("info");
                    child.AddCssClass("glyphicon glyphicon-plus");
                    break;
                case EntryType.FIXED:
                    div.AddCssClass("danger");
                    child.AddCssClass("glyphicon glyphicon-wrench");
                    break;
                case EntryType.UPDATE:
                    div.AddCssClass("warning");
                    child.AddCssClass("glyphicon glyphicon-refresh");
                    break;
                case EntryType.COMPLETED:
                    div.AddCssClass("success");
                    child.AddCssClass("glyphicon glyphicon-thumbs-up");
                    break;
            }

            div.InnerHtml = child.ToString();
            return MvcHtmlString.Create(div.ToString(TagRenderMode.Normal));
        }
    }
}