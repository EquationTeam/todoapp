﻿using System.ComponentModel.DataAnnotations;
using TodoApp.DAL;

namespace TodoApp.ViewModels.TimelineViewModels
{
    public class CreateViewModel
    {
        [Required]
        [Display(Name = "Description")]
        [RegularExpression("^[^<>@#%/]+$", ErrorMessage = "La description ne peut contenir les caractères : < > @ # % /")]
        public string Text { get; set; }
        [Required]
        public EntryType Type { get; set; }
    }
}