﻿using System.ComponentModel.DataAnnotations;

namespace TodoApp.ViewModels.AccountViewModels
{
    public class LoginViewModel
    {
        [Required]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "Le nom d'utilisateur ne peut contenir que des lettres ou des chiffres.")]
        [Display(Name = "Nom d'utilisateur")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression("^[^<>/]+$", ErrorMessage = "Le mot de passe ne peut contenir les caractères : < >")]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }

        [Display(Name = "Mémoriser le mot de passe ?")]
        public bool RememberMe { get; set; }
    }
}