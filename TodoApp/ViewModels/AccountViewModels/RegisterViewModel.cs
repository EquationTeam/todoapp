﻿using System.ComponentModel.DataAnnotations;

namespace TodoApp.ViewModels.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "Le nom d'utilisateur n'est pas valide, ne peut contenir que des lettres ou des chiffres.")]
        [Display(Name = "Nom d'utilisateur")]
        public string UserName { get; set; }

        [Required]
        [RegularExpression("^[^<>/]+$", ErrorMessage = "Le mot de passe ne peut contenir les caractères : < >")]
        [StringLength(100, ErrorMessage = "La chaîne {0} doit comporter au moins {2} caractères.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmer le mot de passe ")]
        [Compare("Password", ErrorMessage = "Le mot de passe et le mot de passe de confirmation ne correspondent pas.")]
        public string ConfirmPassword { get; set; }
    }
}