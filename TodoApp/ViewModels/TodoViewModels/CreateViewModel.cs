﻿using System.ComponentModel.DataAnnotations;
using TodoApp.DAL;

namespace TodoApp.ViewModels.TodoViewModels
{
    public class CreateViewModel
    {
        [Required]
        [Display(Name = "Priorité")]
        public Priority Priority { get; set; }
        [Required]
        [Display(Name = "Description")]
        [RegularExpression("^[^<>@#%/]+$", ErrorMessage = "La description ne peut contenir les caractères : < > @ # % /")]
        public string Text { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_ ]*$", ErrorMessage = "Le titre n'est pas valide, ne peut contenir que des lettres ou des chiffres.")]
        [Display(Name = "Titre")]
        [StringLength(32)]
        public string Title { get; set; }
    }
}