﻿using System.ComponentModel.DataAnnotations;
using TodoApp.DAL;

namespace TodoApp.ViewModels.TodoViewModels
{
    public class EditViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Priorité")]
        public Priority Priority { get; set; }
        [Required]
        [Range(0, 100)]
        [Display(Name = "Progression")]
        public int Progress { get; set; }
        [Required]
        [Display(Name = "Description")]
        [RegularExpression("^[^<>@#%/]+$", ErrorMessage = "La description ne peut contenir les caractères : < > @ # % /")]
        public string Text { get; set; }
        [Required]
        [Display(Name = "Titre")]
        [RegularExpression("^[a-zA-Z0-9_ ]*$", ErrorMessage = "Le titre n'est pas valide, ne peut contenir que des lettres ou des chiffres.")]
        public string Title { get; set; }

        public void Set(Todo model)
        {
            Id = model.Id;
            Priority = model.Priority;
            Progress = model.Progress;
            Text = model.Text;
            Title = model.Title;
        }
    }
}