﻿using System.Web.Mvc;
using TodoApp.DAL;
using TodoApp.Filters;
using TodoApp.ViewModels.TimelineViewModels;
using WebMatrix.WebData;

namespace TodoApp.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class TimelineController : Controller
    {
        private readonly TodoManager todoManager;

        public TimelineController()
        {
            todoManager = new TodoManager();
        }

        //
        // GET: /Timeline/Create/5
        [HttpGet]
        public ActionResult Create(int id)
        {
            ViewBag.Id = id;
            return PartialView();
        }

        //
        // POST: /Timeline/Create/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int id, CreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var timeline = new Timeline()
                {
                    Type = model.Type,
                    Text = model.Text
                };

                todoManager.Attach(WebSecurity.CurrentUserId, id, timeline);
            }
            return RedirectToAction("Details", "Todo", new { id = id });
        }

        //
        // GET: /Timeline/Details/5
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Details(int id)
        {
            var timeline = todoManager.FindTimelineByTodoId(id);
            if (timeline == null)
            {
                return HttpNotFound();
            }
            return PartialView(timeline);
        }
    }
}
