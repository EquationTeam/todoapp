﻿using System.Web.Mvc;
using TodoApp.DAL;
using TodoApp.Filters;
using TodoApp.ViewModels.TodoViewModels;
using WebMatrix.WebData;

namespace TodoApp.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class TodoController : Controller
    {
        private readonly TodoManager todoManager;

        public TodoController()
        {
            todoManager = new TodoManager();
        }

        //
        // GET: /Todo/
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            return View(todoManager.ToList());
        }

        //
        // GET : /Todo/Completed
        [HttpGet]
        public ActionResult Completed(int id)
        {
            Todo todo = todoManager.FindTodoById(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            todoManager.Completed(id);
            return RedirectToAction("Index");
        }

        //
        // GET: /Todo/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Todo/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var todo = new Todo()
                {
                    Title = model.Title,
                    Text = model.Text,
                    Priority = model.Priority
                };

                todoManager.Create(WebSecurity.CurrentUserId, todo);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        //
        // GET: /Todo/Clear
        [HttpGet]
        public ActionResult Clear()
        {
            return View();
        }

        //
        // GET: /Todo/Details
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Details(int id)
        {
            Todo todo = todoManager.FindTodoById(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        //
        // POST: /Todo/Clear
        [HttpPost, ActionName("Clear")]
        [ValidateAntiForgeryToken]
        public ActionResult ClearConfirmed()
        {
            todoManager.Clear();
            return RedirectToAction("Index");
        }

        //
        // GET: /Todo/Edit/5
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            Todo todo = todoManager.FindTodoById(id);
            if (todo == null)
            {
                return HttpNotFound();
            }

            var model = new EditViewModel();
            model.Set(todo);

            return View(model);
        }

        //
        // POST: /Todo/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditViewModel model)
        {
            if (ModelState.IsValid)
            {
                Todo todo = todoManager.FindTodoById(model.Id);
                if (todo == null)
                {
                    return HttpNotFound();
                }
                
                todo.Priority = model.Priority;
                todo.Progress = model.Progress;
                todo.Text = model.Text;
                todo.Title = model.Title;

                todoManager.Edit(todo);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        //
        // GET: /Todo/Delete/5
        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            Todo todo = todoManager.FindTodoById(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        //
        // POST: /Todo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Todo todo = todoManager.FindTodoById(id);
            if (todo == null)
            {
                return HttpNotFound();
            }

            todoManager.Remove(todo);

            return RedirectToAction("Index");
        }
    }
}