@echo off
setlocal

    for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do     rem"') do (
    set "DEL=%%a"
    )

    :: Project
    set PROJECT_NAME=TodoApp
    set PROJECT_CONFIG=%~dp0\.vs\config

    call build.cmd

    if %ERRORLEVEL% NEQ 0 (
        echo.
        goto :error
    )
    
    :: Localisation iisexpress.exe
    set IISEXPRESS_PATH=C:\Program Files\IIS Express

    echo.
    call :colorEcho 0e "Starting %PROJECT_NAME% on IISExpress..."
    echo.

    call "%IISEXPRESS_PATH%\iisexpress.exe" /config:"%PROJECT_CONFIG%\applicationhost.config" /site:%PROJECT_NAME%
    
    :success
    exit /b 0

    :error
    exit /b 1

    :colorEcho
    echo off
    <nul set /p ".=%DEL%" > "%~2"
    findstr /v /a:%1 /R "^$" "%~2" nul
    del "%~2" > nul 2>&1i

endlocal