### Author ###

[Joël de saint Gilles](https://fr.linkedin.com/in/joël-de-saint-gilles-08665812b)

### Clone project with submodule ###

```
git clone --recursive https://muramassaa@bitbucket.org/EquationTeam/todoapp.git
```

### Update submodule command line ###
```
git submodule update --remote --recursive
```

### Install project ###

**For visual studio**

1. Restore package.
2. (Optionnal) Change the TodoContext connection string parameter on all web.config (see below), local db is used by default.
3. Launch

OR 

**For visual studio code or other IDE**

Requirement :

* iisexpress
* NET Framework 4.0.30319

1. Change 'PhysicalPath' to the file './.vs/config/applicationhost.config'.

```
<site name="TodoApp" id="1">
    <application path="/" applicationPool="Clr4IntegratedAppPool">
        <virtualDirectory path="/" physicalPath="PATH_TO_TODO\TodoApp\TodoApp" />
    </application>
    <bindings>
        <binding protocol="http" bindingInformation="*:4399:localhost" />
    </bindings>
</site>
```

2. (Optionnal) Change the TodoContext connection string parameter on all web.config (see below), local db is used by default.
3. Start the cmd "run.cmd" in the current folder.

### Database setting ###

* For Sql server :
```
<connectionStrings>
  <add name="TodoContext" connectionString="Server=127.0.0.1;Database=Todo;Trusted_Connection=True;MultipleActiveResultSets=true" providerName="System.Data.SqlClient" />
</connectionStrings>
```
* For Local DB [Default] :
```
  <connectionStrings>
    <add name="TodoContext" connectionString="Data Source=(LocalDb)\v11.0;Initial Catalog=aspnet-todo-201701291637323;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\aspnet-todo-201701291637323.mdf" providerName="System.Data.SqlClient" />
  </connectionStrings>
```
* Or other provider include package and all requierement.

### Featured ###

* Completed
* Clear
* Delete
* Insert,
* List,
* Migration
* Task timeline
* Update
* User (login, register, change password)

### Requirement ###

* NET Framework 4.5 SDK
* Sql Server (for provider local db)
* Sql Service (for provider sql server)
* Visual Studio 2012 or later

### What was used? ###

* ASP.NET MVC 4
* Boostrap
* EntityFramework

### Preview ###
![screencapture-localhost-4399-1485771485070.png](https://bitbucket.org/repo/RbE4oK/images/3489242654-screencapture-localhost-4399-1485771485070.png)
![screencapture-localhost-4399-Todo-Details-1-1485771507621.png](https://bitbucket.org/repo/RbE4oK/images/1690076371-screencapture-localhost-4399-Todo-Details-1-1485771507621.png)